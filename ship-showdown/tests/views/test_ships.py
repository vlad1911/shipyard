from time import time
from pytest import fixture
from flask import url_for
from app import app
from models.ship import Ship


@fixture
def client():
    with app.test_client() as client:
        with app.app_context():
            yield client


def test_add_product(client):
    url = url_for("product_app.add_ship")
    
    ship_name = "Sidewinder"
    Ship.query.filter_by(name=ship_name).delete()
    
    data = {
        "name": ship_name,
        "mass": 1200,
        "speed": 360,
        "jump": 15,
        "img_id": "1"
    }
    response = client.post(url, data=data, mimetype="application/x-www-form-urlencoded")
    assert response.status_code < 400
    ship = Ship.query.filter_by(name=ship_name).one()
    assert ship.speed == 360